from django.contrib import admin
from .models import Project, ProjectPermission
# Register your models here.

admin.site.register(Project)
admin.site.register(ProjectPermission)