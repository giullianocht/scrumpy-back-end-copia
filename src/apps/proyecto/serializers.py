from rest_framework import serializers

from .models import Project, Member, ProjectRole, ProjectPermission

from autenticacion.serializers import CustomUserSerializer
from autenticacion.models.user import CustomUser

#PROJECT
class CreateProjectSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    name = serializers.CharField()
    description = serializers.CharField()
    scrum_master = serializers.EmailField()
    startDay = serializers.DateField(format="YYYY-MM-DD")
    endDay = serializers.DateField(format="YYYY-MM-DD")

    def save(self):
        scrum_master = self.validated_data.get('scrum_master')
        name = self.validated_data.get('name')
        description = self.validated_data.get('description')
        startDay = self.validated_data.get('startDay')
        endDay = self.validated_data.get('endDay')
        user = CustomUser.objects.get(email=scrum_master)
        project = Project.objects.create(name=name, description=description, scrum_master=user, startDay=startDay, endDay=endDay)
        return project

class ListOrRetrieveProjectSerializer(serializers.ModelSerializer):
    scrum_master = CustomUserSerializer(read_only = True)
    class Meta:
        model = Project
        fields = "__all__"

#PROJECT PERMISSION
class ProjectPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectPermission
        fields = "__all__"

#PROJECT ROLE
class CreateProjectRoleSerializer(serializers.ModelSerializer):
    permissions = serializers.PrimaryKeyRelatedField(
        queryset=ProjectPermission.objects.all(),
        many=True
    )
    class Meta:
        model = ProjectRole
        fields = ['permissions','name','description']

class ListOrRetrieveProjectRoleSerializer(serializers.ModelSerializer):
    project = ListOrRetrieveProjectSerializer(read_only=True)
    permissions = serializers.PrimaryKeyRelatedField(
        queryset=ProjectPermission.objects.all(),
        many=True
    )

    class Meta:
        model = ProjectRole
        fields = "__all__"

class UpdateProjectRoleSerializer(serializers.ModelSerializer):
    permissions = serializers.PrimaryKeyRelatedField(
        queryset=ProjectPermission.objects.all(),
        many=True,
    )
    class Meta:
        model = ProjectRole
        fields = ['permissions','name','description']

#PROJECT MEMBER
class CreateMemberSerializer(serializers.ModelSerializer):
    user = serializers.EmailField()
    
    projectRole = serializers.PrimaryKeyRelatedField(
        queryset=ProjectRole.objects.all(),
    )

    class Meta:
        model = Member
        fields = ['user','projectRole','is_active','availability']

    def save(self):
        email = self.validated_data.get('user')
        projectRole = self.validated_data.get('projectRole')
        project = self.validated_data.get('project')
        is_active = self.validated_data.get('is_active')
        availability = self.validated_data.get('availability')

        user = CustomUser.objects.get(email=email)

        member = Member.objects.create(project=project,user=user,is_active=is_active,projectRole=projectRole,availability=availability)
        return member

class ListOrRetrieveMemberSerializer(serializers.ModelSerializer):
    user = CustomUserSerializer(read_only = True)
    project = ListOrRetrieveProjectSerializer(read_only = True)
    projectRole = ListOrRetrieveProjectRoleSerializer(read_only = True)

    class Meta:
        model = Member
        fields = "__all__"

class UpdateMemberSerializer(serializers.ModelSerializer):
    projectRole = serializers.PrimaryKeyRelatedField(
        queryset=ProjectRole.objects.all(),
    )
    class Meta:
        model = Member
        fields = ['projectRole','is_active','availability']