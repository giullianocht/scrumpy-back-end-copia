from django.db import models

class ProjectStatus(models.TextChoices):
    NEW = 'NEW'
    PENDIENTE = 'PENDIENTE'
    INICIADO = 'INICIADO'
    CANCELADO = 'CANCELADO'
    FINALIZADO = 'FINALIZADO'