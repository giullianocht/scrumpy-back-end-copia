from django.db import models
from .project import Project
from .project_role import ProjectRole
from autenticacion.models import CustomUser

"""
    Modelo Member

    Campos: id, user, rol, is_active, availability
"""

class Member(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    projectRole = models.ForeignKey(ProjectRole, on_delete=models.PROTECT, null=True)
    is_active = models.BooleanField(default=True)
    availability = models.FloatField(default=0, null=True)