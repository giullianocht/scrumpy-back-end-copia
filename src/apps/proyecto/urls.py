from django.urls import path, include

from rest_framework import urlpatterns
from rest_framework.routers import DefaultRouter

from .views import (
    ProjectViewSet, 
    ProjectPermissionViewSet, 
    ProjectRoleView, 
    ProjectRoleRetrieveView, 
    MemberView, 
    MemberRetrieveView,
    )


router = DefaultRouter()

router.register('projects', ProjectViewSet)
router.register('permission', ProjectPermissionViewSet)

urlpatterns = [
    path('projectModule/', include(router.urls)),

    path('projectModule/projects/<int:id_project>/roles/', ProjectRoleView.as_view()),
    path('projectModule/projects/<int:id_project>/roles/<int:id_role>/', ProjectRoleRetrieveView.as_view()),

    path('projectModule/projects/<int:id_project>/members/', MemberView.as_view()),
    path('projectModule/projects/<int:id_project>/members/<int:id_member>/', MemberRetrieveView.as_view()),
]