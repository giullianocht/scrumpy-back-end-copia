from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from sprint.serializers.sprint_serializers import CreateSprintSerializer, UpdateSprintSerializer
from sprint.models import Sprint, EventoSprint
from proyecto.models.project import Project
from sprint.serializers import ListOrRetrieveSprintSerializer
from rest_framework import viewsets, mixins, status, views, permissions

class SprintListView(views.APIView):
    """
    * @api {post} sprintModule/projects/<int:id_project>/sprints/ Crear
    * @apiDescription Crea un nuevo sprint a partir de un campo sprint_goal. 
    Devuelve el Sprint creado con los campos id, project, sprint_goal, status e iteracion.
    * @apiGroup Sprint
    * @apiBody {String} sprint_goal
    * @apiSuccess {Number} id El identificador del sprint creado.
    * @apiSuccess {Object} project Los datos del proyecto al cual pertenece el sprint creado.
    * @apiSuccess {String} sprint_goal  El objetivo a cumplir del sprint.
    * @apiSuccess {String} status  El estado del sprint.
    * @apiSuccess {Number} iteration Un entero secuencial que indica el numero de sprint del proyecto.
    * @apiParamExample {json} Request-Example:
    *     {
    *       "sprint_goal": "Objetivo del Sprint"
    *     }
    @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 201 Created
    *     {
    *       "id": 4,
    *       "project": {
    *           "id": 1,
    *           "scrum_master": 1,
    *           "name": "Proyecto1",
    *           "description": "dewscdsvd",
    *           "status": "NEW",
    *           "startDay": "2021-09-01",
    *           "endDay": "2021-09-25"
    *       },
    *       "sprint_goal": "Objetivo del Sprint",
    *       "status": "EN_PLANIFICACION",
    *       "iteration": 2,
    *       "startDay": null,
    *       "endDay": null
    *     }
    """
    permission_classes = [permissions.IsAuthenticated]
    def post(self, req, id_project):
        serializer = CreateSprintSerializer(data=req.data)
        if serializer.is_valid():
            sprint = serializer.save()
            project = Project.objects.get(pk=id_project)
            sprint.project = project
            sprints = Sprint.objects.filter(project=id_project)
            current_index = len(sprints)
            sprint.iteration=current_index+1
            sprint.save()
            EventoSprint.objects.create(sprint=sprint, description="El usuario {1} creo el Sprint {0}".format(sprint.iteration, req.user.email), user=req.user)
            new_serializer = ListOrRetrieveSprintSerializer(sprint)
            return Response(data=new_serializer.data, status=status.HTTP_201_CREATED)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    """
    * @api {get} sprintModule/projects/<int:id_project>/sprints/ Obtener
    * @apiDescription Obtiene todos los Sprints de un proyecto.
    * @apiGroup Sprint
    * @apiSuccess {Number} id El identificador del sprint creado.
    * @apiSuccess {Object} project Los datos del proyecto al cual pertenece el sprint creado.
    * @apiSuccess {String} sprint_goal  El objetivo a cumplir del sprint.
    * @apiSuccess {String} status  El estado del sprint.
    * @apiSuccess {Number} iteration Un entero secuencial que indica el numero de sprint del proyecto.
    * @apiSuccess {String} startDay La fecha de inicio del sprint.
    * @apiSuccess {String} endDay La fecha de finalización del proyecto.
    @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       {
    *          "id": 1,
    *          "project": {
    *               "id": 1,
    *               "scrum_master": 1,
    *               "name": "Proyecto1",
    *               "description": "dewscdsvd",
    *               "status": "NEW",
    *               "startDay": "2021-09-01",
    *               "endDay": "2021-09-25"
    *          },
    *           "sprint_goal": "Objetivo del Sprint 1",
    *           "status": "EN_PLANIFICACION",
    *           "iteration": 1,
    *           "startDay": null,
    *           "endDay": null
    *       },
    *          "id": 2,
    *          "project": {
    *               "id": 1,
    *               "scrum_master": 1,
    *               "name": "Proyecto1",
    *               "description": "dewscdsvd",
    *               "status": "NEW",
    *               "startDay": "2021-09-01",
    *               "endDay": "2021-09-25"
    *          },
    *           "sprint_goal": "Objetivo del Sprint 2",
    *           "status": "EN_PLANIFICACION",
    *           "iteration": 2,
    *           "startDay": null,
    *           "endDay": null
    *       }
    *     }
    """
    def get(self, req, id_project):
        sprints = Sprint.objects.filter(project=id_project)
        serializer = ListOrRetrieveSprintSerializer(sprints, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class SprintRetrieveView(views.APIView):
    """
    * @api {get} sprintModule/projects/<int:id_project>/sprints/<int:id_sprint>/ Obtener por id
    * @apiDescription Obtiene un Sprint por su id.
    * @apiGroup Sprint
    * @apiSuccess {Number} id El identificador del sprint creado.
    * @apiSuccess {Object} project Los datos del proyecto al cual pertenece el sprint creado.
    * @apiSuccess {String} sprint_goal  El objetivo a cumplir del sprint.
    * @apiSuccess {String} status  El estado del sprint.
    * @apiSuccess {Number} iteration Un entero secuencial que indica el numero de sprint del proyecto.
    * @apiSuccess {String} startDay La fecha de inicio del sprint.
    * @apiSuccess {String} endDay La fecha de finalización del proyecto.
    @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "id": 4,
    *       "project": {
    *           "id": 1,
    *           "scrum_master": 1,
    *           "name": "Proyecto1",
    *           "description": "dewscdsvd",
    *           "status": "NEW",
    *           "startDay": "2021-09-01",
    *           "endDay": "2021-09-25"
    *       },
    *       "sprint_goal": "Objetivo del Sprint",
    *       "status": "EN_PLANIFICACION",
    *       "iteration": 2,
    *       "startDay": null,
    *       "endDay": null
    *     }
    """
    permission_classes = [permissions.IsAuthenticated]
    def get(self, req, id_project, id_sprint):
        sprint = get_object_or_404(Sprint, pk=id_sprint)
        serializer = ListOrRetrieveSprintSerializer(sprint)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    """
    * @api {put} sprintModule/projects/<int:id_project>/sprints/<int:id_sprint>/ Actualizar (put)
    * @apiDescription Permite modificar los campos sprint_goal, status, startDay y endDay de un Sprint.
    Necesita todos los campos del Sprint.
    Devuelve el Sprint Actualizado
    * @apiGroup Sprint
    * @apiBody {Number} id
    * @apiBody {Number} project
    * @apiBody {String} sprint_goal
    * @apiBody {String} status
    * @apiBody {String} startDay
    * @apiBody {String} endDay
    * @apiSuccess {Number} id El identificador del sprint creado.
    * @apiSuccess {Object} project Los datos del proyecto al cual pertenece el sprint creado.
    * @apiSuccess {String} sprint_goal  El objetivo a cumplir del sprint.
    * @apiSuccess {String} status  El estado del sprint.
    * @apiSuccess {Number} iteration Un entero secuencial que indica el numero de sprint del proyecto.
    * @apiSuccess {String} startDay La fecha de inicio del sprint.
    * @apiSuccess {String} endDay La fecha de finalización del proyecto.
    * @apiParamExample {json} Request-Example:
    *       "id": 4,
    *       "project": {
    *           "id": 1,
    *           "scrum_master": 1,
    *           "name": "Proyecto1",
    *           "description": "dewscdsvd",
    *           "status": "NEW",
    *           "startDay": "2021-09-01",
    *           "endDay": "2021-09-25"
    *       },
    *       "sprint_goal": "Objetivo del Sprint Modificado",
    *       "status": "EN_PLANIFICACION",
    *       "iteration": 2,
    *       "startDay": null,
    *       "endDay": null
    *     }
    @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "id": 4,
    *       "project": {
    *           "id": 1,
    *           "scrum_master": 1,
    *           "name": "Proyecto1",
    *           "description": "dewscdsvd",
    *           "status": "NEW",
    *           "startDay": "2021-09-01",
    *           "endDay": "2021-09-25"
    *       },
    *       "sprint_goal": "Objetivo del Sprint Modificado",
    *       "status": "EN_PLANIFICACION",
    *       "iteration": 2,
    *       "startDay": null,
    *       "endDay": null
    *     }
    """
    def put(self, req, id_project, id_sprint):
        serializer = UpdateSprintSerializer(data=req.data)
        sprint = get_object_or_404(Sprint, pk=id_sprint)
        if serializer.is_valid():
            updated_sprint = serializer.update(sprint, serializer.validated_data)
            EventoSprint.objects.create(sprint=updated_sprint, description="El usuario {1} modificó el Sprint {0}".format(sprint.iteration, req.user.email), user=req.user)
            updated_serializer = ListOrRetrieveSprintSerializer(updated_sprint)
            return Response(data=updated_serializer.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    """
    * @api {patch} sprintModule/projects/<int:id_project>/sprints/<int:id_sprint>/ Actualizar (patch)
    * @apiDescription Permite modificar los campos sprint_goal, status, startDay y endDay de un Sprint.
    Necesita el campo, o los campos a actualizar.
    Devuelve el Sprint Actualizado
    * @apiGroup Sprint
    * @apiBody {String} [sprint_goal]   Optional
    * @apiBody {String} [status]    Optional
    * @apiBody {String} [startDay]  Optional
    * @apiBody {String} [endDay]    Optional
    * @apiSuccess {Number} id El identificador del sprint creado.
    * @apiSuccess {Object} project Los datos del proyecto al cual pertenece el sprint creado.
    * @apiSuccess {String} sprint_goal  El objetivo a cumplir del sprint.
    * @apiSuccess {String} status  El estado del sprint.
    * @apiSuccess {Number} iteration Un entero secuencial que indica el numero de sprint del proyecto.
    * @apiSuccess {String} startDay La fecha de inicio del sprint.
    * @apiSuccess {String} endDay La fecha de finalización del proyecto.
    * @apiParamExample {json} Request-Example:
    *     {
    *       "sprint_goal": "Objetivo del Sprint Modificado por patch"
    *     }
    @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "id": 4,
    *       "project": {
    *           "id": 1,
    *           "scrum_master": 1,
    *           "name": "Proyecto1",
    *           "description": "dewscdsvd",
    *           "status": "NEW",
    *           "startDay": "2021-09-01",
    *           "endDay": "2021-09-25"
    *       },
    *       "sprint_goal": "Objetivo del Sprint Modificado por patch",
    *       "status": "EN_PLANIFICACION",
    *       "iteration": 2,
    *       "startDay": null,
    *       "endDay": null
    *     }
    """
    def patch(self, req, id_project, id_sprint):
        serializer = UpdateSprintSerializer(data=req.data, partial=True)
        sprint = get_object_or_404(Sprint, pk=id_sprint)
        if serializer.is_valid():
            updated_sprint = serializer.update(sprint, serializer.validated_data)
            EventoSprint.objects.create(sprint=updated_sprint, description="El usuario {1} modifico el Sprint {0}".format(sprint.iteration, req.user.email), user=req.user)
            updated_serializer = ListOrRetrieveSprintSerializer(updated_sprint)
            return Response(data=updated_serializer.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)