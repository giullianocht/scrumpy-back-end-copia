from rest_framework import views, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from sprint.models.user_story import UserStory
from sprint.serializers.comment_serializers import CreateCommentSerializer, ListOrRetrieveCommentSerializer, UpdateCommentSerializer
from sprint.models import Comment, EventoUserStory
from proyecto.models import Member

class ComentarioListview(views.APIView):
    """
    * @api {post} /sprintModule/projects/:id_project/user_stories/:id_us/comments/ Crear
    * @apiDescription Postea un nuevo comentario para el user story especificado como parámetro.
    * @apiGroup Comment
    * @apiParam {Number} id_project El id del proyecto al cual pertenece el user story.
    * @apiParam {Number} id_us El id del user story sobre el cual se desea comentar.
    * @apiBody {Number} member
    * @apiBody {String} content
    * @apiSuccess {Number} id Un identificador numérico del comentario.
    * @apiSuccess {Object} member Los datos del miembro que posteó el comentario.
    * @apiSuccess {Object} user_story  Los datos del user story al cual fue adjuntado el comentario.
    * @apiSuccess {String} content El contenido del comentario hecho.
    * @apiSuccess {String} created La fecha en la que se posteó el comentario.
    * @apiSuccess {String} modified La fecha de la úlltima modificación hecha sobre el comentario.
    * @apiExample {js} Example usage:
        {
            "member": 1, 
            "content": "Este es el tercer comentario"
        }  
    * @apiSuccessExample {json} Success-Response:
           {
               "id": 3,
               "member": {
                   "id": 1,
                   "user": {
                       "id": 1,
                       "email": "gaonaricardo06@gmail.com",
                       "first_name": "Ricardo",
                       "last_name": "Gaona",
                       "address": "",
                       "phone_number": "",
                       "system_role": null,
                       "is_active": true
                   },
                   "project": {
                       "id": 1,
                       "scrum_master": 1,
                       "name": "Proyecto 1",
                       "description": "Esta es la descripcion del proyecto 1",
                       "status": "NEW",
                       "startDay": "2021-09-01",
                       "endDay": "2022-01-08"
                   },
                   "projectRole": null,
                   "availability": 6.0
               },
               "user_story": {
                   "id": 1,
                   "project": {
                       "id": 1,
                       "scrum_master": 1,
                       "name": "Proyecto 1",
                       "description": "Esta es la descripcion del proyecto 1",
                       "status": "NEW",
                       "startDay": "2021-09-01",
                       "endDay": "2022-01-08"
                   },
                   "sprint": null,
                   "name": "Primer US",
                   "description": "Descripcion del primer US (editado x3)",
                   "priority": 7,
                   "estimate": null,
                   "worked_hours": null,
                   "status": "ARCHIVED",
                   "member": null
               },
               "content": "Este es el tercer comentario",
               "created": "2021-09-18T09:08:22.961355Z",
               "modified": "2021-09-18T09:08:22.961372Z"
           }
    """
    def post(self, req, id_project, id_us):
        serializer = CreateCommentSerializer(data=req.data)
        if serializer.is_valid():
            member_id = serializer.validated_data.get('member')
            content = serializer.validated_data.get('content')
            user_story = UserStory.objects.get(pk=id_us)
            #member = Member.objects.get(pk=member_id)
            saved_comment = Comment.objects.create(member=member_id, user_story=user_story, content=content)
            saved_serializer = ListOrRetrieveCommentSerializer(saved_comment)

            # Se registra el evento de comentar user story
            EventoUserStory.objects.create(
                user_story=user_story, 
                user=req.user, 
                description="El usuario {0} agregó un comentario"
                            .format(req.user.email)
            )
            return Response(data=saved_serializer.data, status=status.HTTP_201_CREATED)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    """
    * @api {get} /sprintModule/projects/:id_project/user_stories/:id_us/comments/ Listar
    * @apiDescription Recupera todos los comentarios de un user story.
    * @apiGroup Comment
    * @apiParam {Number} id_project El id del proyecto al cual pertenece el user story.
    * @apiParam {Number} id_us El id del user story cuyos comentarios se desea obtener.
    * @apiSuccess {Number} id Un identificador numérico del comentario.
    * @apiSuccess {Object} member Los datos del miembro que posteó el comentario.
    * @apiSuccess {Object} user_story  Los datos del user story al cual fue adjuntado el comentario.
    * @apiSuccess {String} content El contenido del comentario hecho.
    * @apiSuccess {String} created La fecha en la que se posteó el comentario.
    * @apiSuccess {String} modified La fecha de la úlltima modificación hecha sobre el comentario.
    * @apiSuccessExample {json} Success-Response:
        [
            {
                "id": 2,
                "member": {
                    "id": 1,
                    "user": {
                        "id": 1,
                        "email": "gaonaricardo06@gmail.com",
                        "first_name": "Ricardo",
                        "last_name": "Gaona",
                        "address": "",
                        "phone_number": "",
                        "system_role": null,
                        "is_active": true
                    },
                    "project": {
                        "id": 1,
                        "scrum_master": 1,
                        "name": "Proyecto 1",
                        "description": "Esta es la descripcion del proyecto 1",
                        "status": "NEW",
                        "startDay": "2021-09-01",
                        "endDay": "2022-01-08"
                    },
                    "projectRole": null,
                    "availability": 6.0
                },
                "user_story": {
                    "id": 1,
                    "project": {
                        "id": 1,
                        "scrum_master": 1,
                        "name": "Proyecto 1",
                        "description": "Esta es la descripcion del proyecto 1",
                        "status": "NEW",
                        "startDay": "2021-09-01",
                        "endDay": "2022-01-08"
                    },
                    "sprint": null,
                    "name": "Primer US",
                    "description": "Descripcion del primer US (editado x3)",
                    "priority": 7,
                    "estimate": null,
                    "worked_hours": null,
                    "status": "ARCHIVED",
                    "member": null
                },
                "content": "Este es el segundo comentario",
                "created": "2021-09-18T08:25:48.212026Z",
                "modified": "2021-09-18T08:25:48.212042Z"
            },
            {
                "id": 3,
                "member": {
                    "id": 1,
                    "user": {
                        "id": 1,
                        "email": "gaonaricardo06@gmail.com",
                        "first_name": "Ricardo",
                        "last_name": "Gaona",
                        "address": "",
                        "phone_number": "",
                        "system_role": null,
                        "is_active": true
                    },
                    "project": {
                        "id": 1,
                        "scrum_master": 1,
                        "name": "Proyecto 1",
                        "description": "Esta es la descripcion del proyecto 1",
                        "status": "NEW",
                        "startDay": "2021-09-01",
                        "endDay": "2022-01-08"
                    },
                    "projectRole": null,
                    "availability": 6.0
                },
                "user_story": {
                    "id": 1,
                    "project": {
                        "id": 1,
                        "scrum_master": 1,
                        "name": "Proyecto 1",
                        "description": "Esta es la descripcion del proyecto 1",
                        "status": "NEW",
                        "startDay": "2021-09-01",
                        "endDay": "2022-01-08"
                    },
                    "sprint": null,
                    "name": "Primer US",
                    "description": "Descripcion del primer US (editado x3)",
                    "priority": 7,
                    "estimate": null,
                    "worked_hours": null,
                    "status": "ARCHIVED",
                    "member": null
                },
                "content": "Este es el tercer comentario",
                "created": "2021-09-18T09:08:22.961355Z",
                "modified": "2021-09-18T09:08:22.961372Z"
            }
        ]
    """
    def get(self, req, id_project, id_us):
        comments = Comment.objects.filter(user_story=id_us)
        serializer = ListOrRetrieveCommentSerializer(comments, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class ComentarioRetrieveView(views.APIView):
    """
    * @api {get} /sprintModule/projects/:id_project/user_stories/:id_us/comments/:id_comment/ Detalle
    * @apiDescription Recupera un comentario específico de un user story.
    * @apiGroup Comment
    * @apiParam {Number} id_project El id del proyecto al cual pertenece el user story.
    * @apiParam {Number} id_us El id del user story cuyos comentarios se desea obtener.
    * @apiParam {Number} id_comment El id del comentario que se desea obtener.
    * @apiSuccess {Number} id Un identificador numérico del comentario.
    * @apiSuccess {Object} member Los datos del miembro que posteó el comentario.
    * @apiSuccess {Object} user_story  Los datos del user story al cual fue adjuntado el comentario.
    * @apiSuccess {String} content El contenido del comentario hecho.
    * @apiSuccess {String} created La fecha en la que se posteó el comentario.
    * @apiSuccess {String} modified La fecha de la úlltima modificación hecha sobre el comentario.
    * @apiSuccessExample {json} Success-Response:
        {
            "id": 2,
            "member": {
                "id": 1,
                "user": {
                    "id": 1,
                    "email": "gaonaricardo06@gmail.com",
                    "first_name": "Ricardo",
                    "last_name": "Gaona",
                    "address": "",
                    "phone_number": "",
                    "system_role": null,
                    "is_active": true
                },
                "project": {
                    "id": 1,
                    "scrum_master": 1,
                    "name": "Proyecto 1",
                    "description": "Esta es la descripcion del proyecto 1",
                    "status": "NEW",
                    "startDay": "2021-09-01",
                    "endDay": "2022-01-08"
                },
                "projectRole": null,
                "availability": 6.0
            },
            "user_story": {
                "id": 1,
                "project": {
                    "id": 1,
                    "scrum_master": 1,
                    "name": "Proyecto 1",
                    "description": "Esta es la descripcion del proyecto 1",
                    "status": "NEW",
                    "startDay": "2021-09-01",
                    "endDay": "2022-01-08"
                },
                "sprint": null,
                "name": "Primer US",
                "description": "Descripcion del primer US (editado x3)",
                "priority": 7,
                "estimate": null,
                "worked_hours": null,
                "status": "ARCHIVED",
                "member": null
            },
            "content": "Este es el segundo comentario",
            "created": "2021-09-18T08:25:48.212026Z",
            "modified": "2021-09-18T08:25:48.212042Z"
        }
    """
    def get(self, req, id_project, id_us, id_comment):
        comment = get_object_or_404(Comment, pk=id_comment)
        serializer = ListOrRetrieveCommentSerializer(comment)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    """
    * @api {put} /sprintModule/projects/:id_project/user_stories/:id_us/comments/:id_comment/ Modificar
    * @apiDescription Permite editar el campo content del comentario y devuelve el mismo comentario ya editado.
    * @apiGroup Comment
    * @apiParam {Number} id_project El id del proyecto al cual pertenece el user story.
    * @apiParam {Number} id_us El id del user story cuyo comentario se desea modificar.
    * @apiParam {Number} id_comment El id del comentario que se desea modificar.
    * @apiBody {String} content
    * @apiSuccess {Number} id Un identificador numérico del comentario.
    * @apiSuccess {Object} member Los datos del miembro que posteó el comentario.
    * @apiSuccess {Object} user_story  Los datos del user story al cual fue adjuntado el comentario.
    * @apiSuccess {String} content El contenido del comentario hecho.
    * @apiSuccess {String} created La fecha en la que se posteó el comentario.
    * @apiSuccess {String} modified La fecha de la úlltima modificación hecha sobre el comentario.
    * @apiSuccessExample {json} Success-Response:
        {
            "id": 2,
            "member": {
                "id": 1,
                "user": {
                    "id": 1,
                    "email": "gaonaricardo06@gmail.com",
                    "first_name": "Ricardo",
                    "last_name": "Gaona",
                    "address": "",
                    "phone_number": "",
                    "system_role": null,
                    "is_active": true
                },
                "project": {
                    "id": 1,
                    "scrum_master": 1,
                    "name": "Proyecto 1",
                    "description": "Esta es la descripcion del proyecto 1",
                    "status": "NEW",
                    "startDay": "2021-09-01",
                    "endDay": "2022-01-08"
                },
                "projectRole": null,
                "availability": 6.0
            },
            "user_story": {
                "id": 1,
                "project": {
                    "id": 1,
                    "scrum_master": 1,
                    "name": "Proyecto 1",
                    "description": "Esta es la descripcion del proyecto 1",
                    "status": "NEW",
                    "startDay": "2021-09-01",
                    "endDay": "2022-01-08"
                },
                "sprint": null,
                "name": "Primer US",
                "description": "Descripcion del primer US (editado x3)",
                "priority": 7,
                "estimate": null,
                "worked_hours": null,
                "status": "ARCHIVED",
                "member": null
            },
            "content": "Este es un comentario editado",
            "created": "2021-09-18T08:25:48.212026Z",
            "modified": "2021-09-18T14:18:40.279956Z"
        }
    """
    def put(self, req, id_project, id_us, id_comment):
        comment = get_object_or_404(Comment, pk=id_comment)
        user_story = get_object_or_404(UserStory, pk=id_us)

        if req.user.id != comment.member.user.id:
            return Response(status=status.HTTP_403_FORBIDDEN)

        serializer = UpdateCommentSerializer(data=req.data)
        if serializer.is_valid():
            updated_comment = serializer.update(comment, serializer.validated_data)
            updated_serializer = ListOrRetrieveCommentSerializer(updated_comment)

            # Se registra el evento de modificar comentario de user story
            EventoUserStory.objects.create(
                user_story=user_story, 
                user=req.user, 
                description="El usuario {0} modificó su comentario"
                            .format(req.user.email)
            )
            return Response(data=updated_serializer.data, status=status.HTTP_200_OK)
        
        return Response(data=serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    """
    * @api {delete} /sprintModule/projects/:id_project/user_stories/:id_us/comments/:id_comment/ Eliminar
    * @apiDescription Permite a un usuario eliminar un comentario suyo, no devuelve ningún json.
    * @apiGroup Comment
    * @apiParam {Number} id_project El id del proyecto al cual pertenece el user story.
    * @apiParam {Number} id_us El id del user story cuyo comentario se desea eliminar.
    * @apiParam {Number} id_comment El id del comentario que se desea eliminar.
    """
    def delete(self, req, id_project, id_us, id_comment):
        comment = get_object_or_404(Comment, pk=id_comment)
        if req.user.id != comment.member.user.id:
            return Response(status=status.HTTP_403_FORBIDDEN)
        user_story = get_object_or_404(UserStory, pk=id_us)
        comment.delete()
        # Se registra el evento de eliminar comentario de user story
        EventoUserStory.objects.create(
            user_story=user_story, 
            user=req.user, 
            description="El usuario {0} eliminó su comentario"
                        .format(req.user.email)
        )
        return Response(status=status.HTTP_200_OK)

class ProjectComments(views.APIView):
    def get(self, req, id_project):
        stories = UserStory.objects.filter(project=id_project)

        comments = []

        for story in stories:
            story_comments = story.comment_set.all()
            for story_comment in story_comments:
                comments.append(story_comment)
            print(comments)

        serializer = ListOrRetrieveCommentSerializer(comments, many=True)

        return Response(data=serializer.data, status=status.HTTP_200_OK)