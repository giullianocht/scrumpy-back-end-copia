from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from sprint.serializers import SprintBacklogSerializer, ListOrRetrieveUserStorySerializer
from sprint.models.user_story import UserStory
from sprint.models.sprint import Sprint
from sprint.models import EventoUserStory
from rest_framework.generics import get_object_or_404

@api_view(['POST'])
def add_story_to_sprint_backlog(req, id_project, id_sprint):
    sprint = get_object_or_404(Sprint, pk=id_sprint)

    backlog_serializer = SprintBacklogSerializer(data=req.data)
    if backlog_serializer.is_valid():
        id_us = backlog_serializer.validated_data.get('id_us')
        user_story = get_object_or_404(UserStory, pk=id_us)
        user_story.sprint = sprint
        user_story.save()

        # Se registra en el historial el paso del US al sprint backlog
        EventoUserStory.objects.create(
            user_story=user_story, 
            user=req.user, 
            description="El usuario {0} agregó el user story al backlog del sprint {1}"
                        .format(req.user.email, sprint.iteration)
        )

        story_serializer = ListOrRetrieveUserStorySerializer(user_story)

        return Response(data=story_serializer.data, status=status.HTTP_200_OK)
    return Response(data=backlog_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
def remove_story_from_sprint_backlog(req, id_project, id_sprint, id_us):
      user_story = get_object_or_404(UserStory, pk=id_us)
      user_story.sprint = None
      user_story.save()
      story_serializer = ListOrRetrieveUserStorySerializer(user_story)
      return Response(data=story_serializer.data, status=status.HTTP_200_OK)