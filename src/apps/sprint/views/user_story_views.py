from rest_framework import viewsets, status, views, permissions
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from apps.sprint.serializers.user_story_serializers import UpdateUserStorySerializer
from proyecto.models.project import Project
from proyecto.models import Member
from scrumpy_backend.utils import enviar_email
from sprint.models.user_story import UserStory
from sprint.models.user_story_status import UserStoryStatus
from sprint.serializers import ListOrRetrieveUserStorySerializer, CreateUserStorySerializer
from sprint.models import EventoUserStory
from django.core.mail import send_mail
from django.conf import settings

class UserStoryListView(views.APIView):

    """
    * @api {post} /sprintModule/projects/:id_project/user_story/ Crear
    * @apiDescription Crea un nuevo user story.
    * @apiGroup User story
    * @apiParam {Number} id_project El id del proyecto al cual se desea agregar un user story.
    * @apiBody {String} name
    * @apiBody {String} description
    * @apiBody {Number} priority
    * @apiSuccess {String} id Número de identificación único del user story.
    * @apiSuccess {String} project Los datos del proyecto al cual pertenece el user story creado.
    * @apiSuccess {String} sprint Los datos del sprint cuando el user story pertenezca a uno.
    * @apiSuccess {String} name El nombre del user story.
    * @apiSuccess {String} description  Una breve descripción del user story.
    * @apiSuccess {String} priority Un entero del 1 al 10 que especifica la prioridad del user story.
    * @apiSuccess {String} estimate Un entero del 1 al 10 que especifica la estimación en story points del user story.
    * @apiSuccess {String} worked_hours Las horas reales que se emplearon para terminar el user story.
    * @apiSuccess {String} status El estado en el que se encuentra el user story (NEW, TODO, DOING, DONE, UNFINISHED, REJECTED, ARCHIVED).
    * @apiSuccess {String} member Los datos del miembro del equipo al que se le asignó el user story.
    * @apiExample {js} Example body:
        {
            "name": "Funcionalidad de PIN",
            "description": "Solicitar un PIN al usuario cada vez que ingresa a la aplicación",
            "priority": 8
        }
    * @apiSuccessExample {json} Success-Response:
        {
            "id": 3,
            "project": {
                "id": 1,
                "scrum_master": 1,
                "name": "Proyecto 1",
                "description": "Esta es la descripcion del proyecto 1",
                "status": "NEW",
                "startDay": "2021-09-01",
                "endDay": "2022-01-08"
            },
            "sprint": null,
            "name": "Funcionalidad de PIN",
            "description": "Solicitar un PIN al usuario cada vez que ingresa a la aplicación",
            "priority": 8,
            "estimate": null,
            "worked_hours": null,
            "status": "NEW",
            "member": null
        }
    """
    permission_classes = [permissions.IsAuthenticated, ]
    def post(self, req, id_project):
        serializer = CreateUserStorySerializer(data=req.data)
        if serializer.is_valid():
            user_story = serializer.save()
            project = Project.objects.get(pk=id_project)
            user_story.project = project
            user_story.save()

            # Se registra el evento de creacion del user story
            EventoUserStory.objects.create(
                user_story=user_story, 
                user=req.user, 
                description="Se crea el user story por el usuario {0} y se lo agrega al product backlog del proyecto con id = {1}"
                            .format(req.user.email, id_project)
            )
            new_serializer = ListOrRetrieveUserStorySerializer(user_story)
            return Response(data=new_serializer.data, status=status.HTTP_201_CREATED)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    """
    * @api {get} /sprintModule/projects/:id_project/user_story/ Listar
    * @apiDescription Lista todos los user stories de un proyecto.
    * @apiGroup User story
    * @apiParam {Number} id_project El id del proyecto cuyos user stories se desean listar.
    * @apiSuccess {String} id Número de identificación único del user story.
    * @apiSuccess {String} project Los datos del proyecto al cual pertenece el user story creado.
    * @apiSuccess {String} sprint Los datos del sprint cuando el user story pertenezca a uno.
    * @apiSuccess {String} name El nombre del user story.
    * @apiSuccess {String} description  Una breve descripción del user story.
    * @apiSuccess {String} priority Un entero del 1 al 10 que especifica la prioridad del user story.
    * @apiSuccess {String} estimate Un entero del 1 al 10 que especifica la estimación en story points del user story.
    * @apiSuccess {String} worked_hours Las horas reales que se emplearon para terminar el user story.
    * @apiSuccess {String} status El estado en el que se encuentra el user story (NEW, TODO, DOING, DONE, UNFINISHED, REJECTED, ARCHIVED).
    * @apiSuccess {String} member Los datos del miembro del equipo al que se le asignó el user story.
    * @apiSuccessExample {json} Success-Response:
        [
            {
                "id": 1,
                "project": {
                    "id": 1,
                    "scrum_master": 1,
                    "name": "Proyecto 1",
                    "description": "Esta es la descripcion del proyecto 1",
                    "status": "NEW",
                    "startDay": "2021-09-01",
                    "endDay": "2022-01-08"
                },
                "sprint": null,
                "name": "Primer US",
                "description": "Descripcion del primer US (editado x3)",
                "priority": 7,
                "estimate": null,
                "worked_hours": null,
                "status": "ARCHIVED",
                "member": null
            },
            {
                "id": 2,
                "project": {
                    "id": 1,
                    "scrum_master": 1,
                    "name": "Proyecto 1",
                    "description": "Esta es la descripcion del proyecto 1",
                    "status": "NEW",
                    "startDay": "2021-09-01",
                    "endDay": "2022-01-08"
                },
                "sprint": null,
                "name": "Segundo US",
                "description": "Descripcion del segundo US",
                "priority": 3,
                "estimate": null,
                "worked_hours": null,
                "status": "NEW",
                "member": null
            }
        ]
    """

    def get(self, req, id_project):
        stories = UserStory.objects.filter(project=id_project)
        serializer = ListOrRetrieveUserStorySerializer(stories, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class UserStoryRetrieveView(views.APIView):

    """
    * @api {get} /sprintModule/projects/:id_project/user_story/:id_us/ Detalle
    * @apiDescription obtiene un user story específico del projecto.
    * @apiGroup User story
    * @apiParam {Number} id_project El id del proyecto cuyo user story se desea obtener.
    * @apiParam {Number} id_us El id del user story que se desea obtener.
    * @apiSuccess {String} id Número de identificación único del user story.
    * @apiSuccess {String} project Los datos del proyecto al cual pertenece el user story creado.
    * @apiSuccess {String} sprint Los datos del sprint cuando el user story pertenezca a uno.
    * @apiSuccess {String} name El nombre del user story.
    * @apiSuccess {String} description  Una breve descripción del user story.
    * @apiSuccess {String} priority Un entero del 1 al 10 que especifica la prioridad del user story.
    * @apiSuccess {String} estimate Un entero del 1 al 10 que especifica la estimación en story points del user story.
    * @apiSuccess {String} worked_hours Las horas reales que se emplearon para terminar el user story.
    * @apiSuccess {String} status El estado en el que se encuentra el user story (NEW, TODO, DOING, DONE, UNFINISHED, REJECTED, ARCHIVED).
    * @apiSuccess {String} member Los datos del miembro del equipo al que se le asignó el user story.
    * @apiSuccessExample {json} Success-Response:
        {
            "id": 1,
            "project": {
                "id": 1,
                "scrum_master": 1,
                "name": "Proyecto 1",
                "description": "Esta es la descripcion del proyecto 1",
                "status": "NEW",
                "startDay": "2021-09-01",
                "endDay": "2022-01-08"
            },
            "sprint": null,
            "name": "Primer US",
            "description": "Descripcion del primer US (editado x3)",
            "priority": 7,
            "estimate": null,
            "worked_hours": null,
            "status": "ARCHIVED",
            "member": null
        }
    """
    def get(self, req, id_project, id_us):
        story = get_object_or_404(UserStory, pk=id_us)
        serializer = ListOrRetrieveUserStorySerializer(story)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    """
    * @api {put} /sprintModule/projects/:id_project/user_story/:id_us/ Modificar (put)
    * @apiDescription Permite editar todos los campos de un user story.
    * @apiGroup User story
    * @apiParam {Number} id_project El id del proyecto al cual se desea agregar un user story.
    * @apiParam {Number} id_us El id del user story que se desea editar.
    * @apiBody {String} project
    * @apiBody {String} sprint
    * @apiBody {String} name
    * @apiBody {String} description
    * @apiBody {String} priority
    * @apiBody {String} estimate
    * @apiBody {String} worked_hours
    * @apiBody {String} status (NEW, TODO, DOING, DONE, UNFINISHED, REJECTED, ARCHIVED).
    * @apiBody {String} member
    * @apiSuccess {String} id Número de identificación único del user story.
    * @apiSuccess {String} project Los datos del proyecto al cual pertenece el user story creado.
    * @apiSuccess {String} sprint Los datos del sprint cuando el user story pertenezca a uno.
    * @apiSuccess {String} name El nombre del user story.
    * @apiSuccess {String} description  Una breve descripción del user story.
    * @apiSuccess {String} priority Un entero del 1 al 10 que especifica la prioridad del user story.
    * @apiSuccess {String} estimate Un entero del 1 al 10 que especifica la estimación en story points del user story.
    * @apiSuccess {String} worked_hours Las horas reales que se emplearon para terminar el user story.
    * @apiSuccess {String} status El estado en el que se encuentra el user story (NEW, TODO, DOING, DONE, UNFINISHED, REJECTED, ARCHIVED).
    * @apiSuccess {String} member Los datos del miembro del equipo al que se le asignó el user story.
    * @apiExample {js} Example body:
        {
            "project": null,
            "sprint": 3,
            "name": "Primer US",
            "description": "Descripcion del primer US (editado)",
            "priority": 6,
            "estimate": null,
            "worked_hours": 12,
            "status": "DONE",
            "member": 3
        }
    * @apiSuccessExample {json} Success-Response:
        {
            "id": 1,
            "project": {
                "id": 2,
                "scrum_master": 2,
                "name": "Proyecto 2",
                "description": "Esta es la descripcion del proyecto 2",
                "status": "NEW",
                "startDay": "2021-09-01",
                "endDay": "2022-01-08"
            },
            "sprint": null,
            "name": "Primer US",
            "description": "Descripcion del primer US (editado)",
            "priority": 6,
            "estimate": null,
            "worked_hours": 12,
            "status": "DONE",
            "member": 3
        }
    """

    def put(self, req, id_project, id_us):
        serializer = UpdateUserStorySerializer(data=req.data)
        story = get_object_or_404(UserStory, pk=id_us)
        
        if serializer.is_valid():
            updated_story = serializer.update(story, serializer.validated_data)
            updated_serializer = ListOrRetrieveUserStorySerializer(updated_story)
            # Se registra el evento de modificacion del user story
            EventoUserStory.objects.create(
                user_story=updated_story, 
                user=req.user, 
                description="Se modifica el user story por el usuario {0}"
                            .format(req.user.email)
            )
            return Response(data=updated_serializer.data, status=status.HTTP_200_OK)
        
        return Response(data=serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    """
    * @api {patch} /sprintModule/projects/:id_project/user_story/:id_us/ Modificar (patch)
    * @apiDescription Permite editar uno o más campos de un user story.
    * @apiGroup User story
    * @apiParam {Number} id_project El id del proyecto al cual se desea agregar un user story.
    * @apiParam {Number} id_us El id del user story que se desea editar.
    * @apiBody {String} [project]
    * @apiBody {String} [sprint]
    * @apiBody {String} [name]
    * @apiBody {String} [description]
    * @apiBody {String} [priority]
    * @apiBody {String} [estimate]
    * @apiBody {String} [worked_hours]
    * @apiBody {String} [status] (NEW, TODO, DOING, DONE, UNFINISHED, REJECTED, ARCHIVED).
    * @apiBody {String} [member]
    * @apiSuccess {String} id Número de identificación único del user story.
    * @apiSuccess {String} project Los datos del proyecto al cual pertenece el user story creado.
    * @apiSuccess {String} sprint Los datos del sprint cuando el user story pertenezca a uno.
    * @apiSuccess {String} name El nombre del user story.
    * @apiSuccess {String} description  Una breve descripción del user story.
    * @apiSuccess {String} priority Un entero del 1 al 10 que especifica la prioridad del user story.
    * @apiSuccess {String} estimate Un entero del 1 al 10 que especifica la estimación en story points del user story.
    * @apiSuccess {String} worked_hours Las horas reales que se emplearon para terminar el user story.
    * @apiSuccess {String} status El estado en el que se encuentra el user story (NEW, TODO, DOING, DONE, UNFINISHED, REJECTED, ARCHIVED).
    * @apiSuccess {String} member Los datos del miembro del equipo al que se le asignó el user story.
    * @apiExample {js} Example body:
        {
            "description": "Descripcion del primer US (editado nuevamente)",
        }
    * @apiSuccessExample {json} Success-Response:
        {
            "id": 1,
            "project": {
                "id": 2,
                "scrum_master": 2,
                "name": "Proyecto 2",
                "description": "Esta es la descripcion del proyecto 2",
                "status": "NEW",
                "startDay": "2021-09-01",
                "endDay": "2022-01-08"
            },
            "sprint": null,
            "name": "Primer US",
            "description": "Descripcion del primer US (editado nuevamente)",
            "priority": 6,
            "estimate": null,
            "worked_hours": 12,
            "status": "DONE",
            "member": 3
        }
    """
    def patch(self, req, id_project, id_us):
        serializer = UpdateUserStorySerializer(data=req.data, partial=True)
        story = get_object_or_404(UserStory, pk=id_us)
        
        if serializer.is_valid():
            updated_story = serializer.update(story, serializer.validated_data)
            updated_serializer = ListOrRetrieveUserStorySerializer(updated_story)

            # Se registra el evento de modificacion del user story
            EventoUserStory.objects.create(
                user_story=updated_story, 
                user=req.user, 
                description="Se modifica el user story por el usuario {0}"
                            .format(req.user.email)
            )

            if serializer.validated_data.get('member'):
                print('Enviar email')
                member = serializer.validated_data.get('member')

                enviar_email(
                    subject="Notificaciones del proyecto",
                    content="Se te ha asignado el user story: {}\n".format(story.name),
                    email_to=member.user.email
                )

            return Response(data=updated_serializer.data, status=status.HTTP_200_OK)
        
        return Response(data=serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    """
    * @api {delete} /sprintModule/projects/:id_project/user_stories/:id_us/ Archivar
    * @apiDescription Permite a un usuario archivar un user story.
    * @apiGroup User story
    * @apiParam {Number} id_project El id del proyecto al cual pertenece el user story que se desea archivar, no devuelve ningún json.
    * @apiParam {Number} id_us El id del user story que se desea archivar.
    """
    def delete(self, req, id_project, id_us):
        story = get_object_or_404(UserStory, pk=id_us)
        story.status = UserStoryStatus.ARCHIVED
        story.save()

        serializer = ListOrRetrieveUserStorySerializer(story)

        # Se registra el evento de archivar user story
        EventoUserStory.objects.create(
            user_story=story, 
            user=req.user, 
            description="Se archiva el user story por el usuario {0}"
                        .format(req.user.email)
        )

        return Response(data=serializer.data, status=status.HTTP_200_OK)
