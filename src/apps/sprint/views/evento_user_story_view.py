from rest_framework.response import Response
from rest_framework import views
from sprint.models.evento_user_story import EventoUserStory
from sprint.serializers import EventoUserStorySerializer


class EventoUserStoryApiView(views.APIView):
    """
    * @api {get} /sprintModule/projects/:id_project/user_story/:id_us/history Listar Historial de Eventos de User Story
    * @apiDescription Lista todos los eventos de los user stories de un proyecto.
    * @apiName GetHistorial
    * @apiGroup Historial
    * @apiParam {Number} id_project El id del proyecto cuyo historial user stories se desean listar.
    * @apiParam {Number} id_us El id del User Story cuyo historial user stories se desean listar.
    * @apiSuccess {String} id Número de identificación único del user story.
    * @apiSuccess {String} project Los datos del proyecto al cual pertenece el user story creado.
    * @apiSuccess {String} sprint Los datos del sprint cuando el user story pertenezca a uno.
    * @apiSuccess {String} name El nombre del user story.
    * @apiSuccess {String} description  Una breve descripción del user story.
    * @apiSuccess {String} priority Un entero del 1 al 10 que especifica la prioridad del user story.
    * @apiSuccess {String} estimate Un entero del 1 al 10 que especifica la estimación en story points del user story.
    * @apiSuccess {String} worked_hours Las horas reales que se emplearon para terminar el user story.
    * @apiSuccess {String} status El estado en el que se encuentra el user story (NEW, TODO, DOING, DONE, UNFINISHED, REJECTED, ARCHIVED).
    * @apiSuccess {String} member Los datos del miembro del equipo al que se le asignó el user story.
    * @apiSuccessExample {json} Success-Response:
        [
            {
                "id": 1,
                "project": {
                    "id": 1,
                    "scrum_master": 1,
                    "name": "Proyecto 1",
                    "description": "Esta es la descripcion del proyecto 1",
                    "status": "NEW",
                    "startDay": "2021-09-01",
                    "endDay": "2022-01-08"
                },
                "sprint": null,
                "name": "Primer US",
                "description": "Descripcion del primer US (editado x3)",
                "priority": 7,
                "estimate": null,
                "worked_hours": null,
                "status": "ARCHIVED",
                "member": null
            },
            {
                "id": 2,
                "project": {
                    "id": 1,
                    "scrum_master": 1,
                    "name": "Proyecto 1",
                    "description": "Esta es la descripcion del proyecto 1",
                    "status": "NEW",
                    "startDay": "2021-09-01",
                    "endDay": "2022-01-08"
                },
                "sprint": null,
                "name": "Segundo US",
                "description": "Descripcion del segundo US",
                "priority": 3,
                "estimate": null,
                "worked_hours": null,
                "status": "NEW",
                "member": null
            }
        ]
    """
    def get(self, request, id_project, id_us):
        evento_user_story = EventoUserStory.objects.filter(user_story=id_us)
        eventJson = EventoUserStorySerializer(evento_user_story, many=True)
        return self.response(eventJson.data)

    def response(self, data, status=200):
        return Response(data, status=status)