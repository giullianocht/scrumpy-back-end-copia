from django.db import models
from proyecto.models import Member
from sprint.models.user_story import UserStory

class Comment(models.Model):
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    user_story = models.ForeignKey(UserStory, on_delete=models.CASCADE)
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)