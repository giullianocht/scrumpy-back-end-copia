
from django.db import models

from sprint.models import Sprint
from autenticacion.models import CustomUser
from django.utils import timezone

class EventoSprint(models.Model):
    sprint = models.ForeignKey(Sprint, on_delete=models.PROTECT, null=True)
    description = models.TextField(default="")
    user = models.ForeignKey(CustomUser, on_delete=models.PROTECT, null=True)
    date = models.DateField(default=timezone.now)
    
    def __str__(self):
        return self.description