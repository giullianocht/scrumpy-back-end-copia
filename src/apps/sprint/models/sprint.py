
from django.db import models
from sprint.models.sprint_status import SprintStatus
from proyecto.models import Project


class Sprint(models.Model):
    sprint_goal = models.CharField(max_length=100, null=True, blank=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    status = models.CharField(
        max_length=16,
        choices=SprintStatus.choices,
        default=SprintStatus.EN_PLANIFICACION,
        null=True, blank=True
    )
    iteration = models.IntegerField(null=True, blank=True)
    startDay = models.DateField(null=True, blank=True)
    endDay = models.DateField(null=True, blank=True)
