from django.urls import path
from sprint.views import (
    UserStoryListView, 
    UserStoryRetrieveView, 
    ComentarioListview,
    ComentarioRetrieveView, 
    SprintListView, 
    SprintRetrieveView, 
    EventoUserStoryApiView,
    EventoSprintApiView,
    ProjectComments,
    add_story_to_sprint_backlog,
    remove_story_from_sprint_backlog,
)

urlpatterns = [
    path('sprintModule/projects/<int:id_project>/user_stories/', UserStoryListView.as_view()),
    path('sprintModule/projects/<int:id_project>/user_stories/<int:id_us>/', UserStoryRetrieveView.as_view()),
    path('sprintModule/projects/<int:id_project>/user_stories/<int:id_us>/history/', EventoUserStoryApiView.as_view()),
    path('sprintModule/projects/<int:id_project>/sprints/<int:id_sprint>/history/', EventoSprintApiView.as_view()),
    path('sprintModule/projects/<int:id_project>/user_stories/<int:id_us>/comments/', ComentarioListview.as_view()),
    path('sprintModule/projects/<int:id_project>/user_stories/<int:id_us>/comments/<int:id_comment>/', ComentarioRetrieveView.as_view()),
    path('sprintModule/projects/<int:id_project>/sprints/', SprintListView.as_view()),
    path('sprintModule/projects/<int:id_project>/sprints/<int:id_sprint>/', SprintRetrieveView.as_view()),
    path('sprintModule/projects/<int:id_project>/comments/', ProjectComments.as_view()),
    path('sprintModule/projects/<int:id_project>/sprints/<int:id_sprint>/backlog/', add_story_to_sprint_backlog),
    path('sprintModule/projects/<int:id_project>/sprints/<int:id_sprint>/backlog/<int:id_us>/', remove_story_from_sprint_backlog),
]
