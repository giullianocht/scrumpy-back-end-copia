from rest_framework import serializers
from sprint.models.evento_user_story import EventoUserStory

class EventoUserStorySerializer(serializers.ModelSerializer):
    class Meta:
        model = EventoUserStory
        fields = "__all__"