from rest_framework import serializers
from proyecto.models.member import Member
from proyecto.models import Project
from sprint.models.sprint import Sprint
from sprint.models.user_story import UserStory
from proyecto.serializers import ListOrRetrieveProjectSerializer
from sprint.serializers.sprint_serializers import ListOrRetrieveSprintSerializer

class CreateUserStorySerializer(serializers.ModelSerializer):
    class Meta:
        model=UserStory
        fields=['name', 'description', 'priority']

class ListOrRetrieveUserStorySerializer(serializers.ModelSerializer):
    project = ListOrRetrieveProjectSerializer(read_only=True)
    sprint = ListOrRetrieveSprintSerializer(read_only=True)
    class Meta:
        model=UserStory
        fields="__all__"

class UpdateUserStorySerializer(serializers.ModelSerializer):
    project = serializers.PrimaryKeyRelatedField(
        queryset=Project.objects.all()
    )
    sprint = serializers.PrimaryKeyRelatedField(
        queryset=Sprint.objects.all(), allow_null=True
    )
    member = serializers.PrimaryKeyRelatedField(
        queryset=Member.objects.all(), allow_null=True
    )
    class Meta:
        model=UserStory
        fields="__all__"