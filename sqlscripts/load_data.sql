-- Insertamos todos los permisos a nivel de sistema
insert into 
    autenticacion_systempermission (code, name, description) 
values 
    (100, 'Iniciar sesión', 'Permite al usuario iniciar sesión en el sistema'),
    (200, 'Registrar usuario', 'Permite al usuario iniciar sesión en el sistema'),
    (201, 'Modificar usuario', 'Permite al usuario iniciar sesión en el sistema'),
    (300, 'Crear proyecto', 'Permite al usuario iniciar sesión en el sistema'),
    (301, 'Modificar proyecto', 'Permite al usuario iniciar sesión en el sistema'),
    (400, 'Crear rol', 'Permite al usuario iniciar sesión en el sistema'),
    (401, 'Modificar rol', 'Permite al usuario iniciar sesión en el sistema'),
    (402, 'Eliminar rol', 'Permite al usuario iniciar sesión en el sistema');

-- Insertamos el rol de administrador
insert into 
    autenticacion_systemrole (name, description)
values 
    ('Administrador', 'Tiene control total sobre el sistema');

-- Asociamos todos los permisos al rol administrador
insert into 
    autenticacion_systemrole_permissions (systemrole_id, systempermission_id) 
values 
    (1,1), 
    (1,2), 
    (1,3), 
    (1,4), 
    (1,5), 
    (1,6), 
    (1,7), 
    (1,8);

-- Asignamos al super user el rol administrador
update autenticacion_customuser set system_role_id=1 where id=1;

-- Insertamos todos los permisos a nivel de proyecto
insert into 
    proyecto_projectpermission (code, name, description) 
values 
    (100, 'Crear user story', 'Permite al usuario agregar un nuevo user story'),
    (101, 'Modificar user story', 'Permite al usuario modificar un user story existente'),
    (102, 'Asignar user story', 'Permite al usuario asignar un responsable al user story'),
    (103, 'Estimar user story', 'Permite al usuario agregar una estimación al user story'),
    (104, 'Archivar user story', 'Permite al usuario archivar un user story'),
    (200, 'Crear sprint', 'Permite al usuario crear un nuevo sprint'),
    (201, 'Planificar sprint', 'Permite al usuario agregar user stories al sprint backlog'),
    (202, 'Iniciar sprint', 'Permite al usuario iniciar un sprint planificado'),
    (203, 'Finalizar sprint', 'Permite al usuario finalizar en cualquier momento un sprint'),
    (300, 'Agregar miembro', 'Permite al usuario agregar un nuevo miembro al proyecto'),
    (301, 'Modificar miembro', 'Permite al usuario modificar un miembro existente'),
    (302, 'Eliminar miembro', 'Permite al usuario eliminar un miembro que no se encuentre en un sprint en curso'),
    (400, 'Crear rol de proyecto', 'Permite al usuario crear un rol con permisos a nivel de proyecto'),
    (401, 'Modificar rol de proyecto', 'Permite al usuario modificar detalles y permisos de un rol de proyecto'),
    (500, 'Editar las configuraciones', 'Permite al usuario editar las configuraciones del proyecto');
